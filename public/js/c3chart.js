var chart = c3.generate({
    bindto: '#chart',
    data: {
    	type: 'pie',
      columns: [
        ['Excellent', 73],
        ['Good', 23],
        ['Improve', 6],
      ]
    }
});

var chart2 = c3.generate({
    bindto: '#chart2',
    data: {
        type: 'pie',
      columns: [
        ['Venue', 175],
        ['Food', 1025],
        ['Decoration', 91],
        ['Bathukamma', 115],
        ['Bank and Other Fee', 66],
        ['Kolatam Sticks', 149],
        ['Flowers', 115]
      ]
    },
    pie: {
      label: {
        format: function(value, ratio, id) {
          return value+ "€";
        },

        // Threshold that shows all slices even very small
        threshold: 0.01
      }
    }
});

// Please pay attention to the month (parts[1]); JavaScript counts months from 0:
// January - 0, February - 1, etc.
var daysLeft = function(eventdate) {
    var parts =eventdate.split('.');
    var inputDate = new Date(parts[2], parts[1] - 1, parts[0]);
    var today = new Date();
    var timeDiff = Math.abs(inputDate.getTime() - today.getTime());
    return "(" + eventdate + ") " + Math.ceil(timeDiff / (1000*3600*24)) + " days left";
};

document.getElementById("event1").innerHTML = daysLeft("12.01.2020");
document.getElementById("event2").innerHTML = daysLeft("15.1.2020");
//document.getElementById("event3").innerHTML = daysLeft("15.1.2020");
